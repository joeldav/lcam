import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Login } from './login/view/login/login';
import {FormsModule} from "@angular/forms";
import {LoginForm} from "./login/components/forms/login/loginForm";
import {FacebookLoginButton} from "./login/components/forms/fb/loginButton";
import {HttpClientModule} from "@angular/common/http";
import {AuthGuard} from "./login/services/authGuard";

@NgModule({
  declarations: [
    AppComponent,
    Login,
    LoginForm,
    FacebookLoginButton
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
