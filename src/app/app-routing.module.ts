import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Success} from "./login/view/success/success";
import {AuthGuard} from "./login/services/authGuard";
import {Login} from "./login/view/login/login";

const routes: Routes = [
  {path:'', redirectTo: 'login', pathMatch: 'full'},
  { path: 'login', component: Login },
  { path: 'success',
    component: Success,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
