import {Component, EventEmitter, Output} from '@angular/core';
import {User} from "../../../models/user";
import {AuthenticationFactory} from "../../../services/authentication/authentication";

@Component({
  selector: 'login-form',
  template: `
    <form (ngSubmit)="handleLogin(form.value, form.valid)" #form="ngForm" novalidate>
      <fieldset>
        <legend>Login</legend>
        <div class="form-group">
          <input type="text" placeholder="Username" name="username" #username="ngModel" [ngModel]="user?.username"
                 required/>
          <div *ngIf="username.errors?.required && username.dirty">Please enter a username</div>
        </div>
        <div class="form-group">
          <input type="password" placeholder="Password" name="password" #password="ngModel" [ngModel]="user?.password"
                 required/>
          <div *ngIf="password.errors?.required && password.dirty">Please enter a password</div>
        </div>
        <button type="submit" [disabled]="form.invalid" class="btn btn-primary">Login</button>
      </fieldset>
      <div *ngIf="error">{{error}}</div>
    </form>
  `
})
export class LoginForm {
  public user: User = {username: '', password: ''};
  private submitted = false;
  private authenticationService;
  public error;

  @Output()
  update: EventEmitter<any> = new EventEmitter();

  constructor(private auth: AuthenticationFactory) {
    this.authenticationService = auth.create('db');
  }


  handleLogin(form, valid: boolean) {
    this.submitted = true;
    this.error = '';
    if (!valid) {
      return;
    }
    this.authenticationService.login(form.username.value, form.password.value)
      .then(() => {
        this.update.emit(true);
      });

  }
}

