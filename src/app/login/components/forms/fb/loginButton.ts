import {Component, EventEmitter, Output} from '@angular/core';
import {AuthenticationFactory} from "../../../services/authentication/authentication";

@Component({
  selector: 'fb-login-button',
  template: `
    <button class="btn-secondary mt-5" (click)="login()">Sign in with Facebook</button>
  `,
  providers: [AuthenticationFactory]
})
export class FacebookLoginButton {
  @Output()
  update: EventEmitter<any> = new EventEmitter();

  private fb;

  constructor(private auth: AuthenticationFactory) {

    this.fb = auth.create('facebook');
    this.fb.setAppId("1567760930100517");

  }

  login() {
    this.fb.login().then(() => this.update.emit(true)).catch(() => {
      console.error('User did not login with Facebook');
    });
  }

}
