export interface authenticationService {
  login(username?: string, password?: string): any;
}
