import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {AuthenticationFactory} from "../../services/authentication/authentication";

@Component({
  selector: 'page-login',
  template: `
    <div>
      <login-form (update)="onLoggedIn($event)"></login-form>
      <fb-login-button (update)="onLoggedIn($event)"></fb-login-button>
    </div>`,
  providers: [AuthenticationFactory]
})
export class Login {

  constructor(private router: Router) {
  }

  onLoggedIn(value: boolean) {
    if (value) {
      this.router.navigate(['/success']);
    }
  }

}
