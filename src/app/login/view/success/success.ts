import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'page-success',
  template: `<p>You successfully logged in</p>`
})
export class Success implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
