import {Injectable} from '@angular/core';
import {authenticationService} from "../../../models/authentication";

declare var FB:any;

@Injectable()
export class facebookService implements authenticationService {
  private applicationId:string;

  setAppId(appId) {
    this.applicationId = appId;
  }

  public init() {
    return new Promise((resolve, reject) => {
      if (typeof FB !== 'undefined') {
        resolve();
      } else {
        (<any>window).fbAsyncInit = () => {
          FB.init({
            appId: this.applicationId,
            xfbml: false,
            version: 'v7.0'
          });
          resolve();
        };

        (function (d, s, id) {
          let js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) { return; }
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-js-sdk'));
      }
    });
  }

  public getStatus() {
    return new Promise((resolve, reject) => {
      FB.getLoginStatus(function (response) {
        resolve(response);
      });
    });
  }

  public storeToken(token:string) {
    localStorage.setItem('id_token', token);
  }

  public login() {
    return new Promise((resolve, reject) => {
      this.init().then(() => this.getStatus())
        .then((res: any) => {
          if (res.status !== 'connected') {
            FB.login((result: any) => {
              if (result.status === 'connected') {
                this.storeToken(result.authResponse.accessToken);
                return resolve();
              } else {
                return reject();
              }
            });
          } else {
            this.storeToken(res.authResponse.accessToken);
            return resolve();
          }
        })
    });
  }

}
