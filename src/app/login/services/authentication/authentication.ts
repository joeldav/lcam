import {Injectable} from '@angular/core';
import {facebookService} from "./adapter/facebook";
import {backendService} from "./adapter/backend";
import {authenticationService} from "../../models/authentication";
import {map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";

export abstract class Authentication {
}

@Injectable()
export class AuthenticationFactory extends Authentication {
  constructor(private http: HttpClient) {
    super();
  }

  create(type): authenticationService {
    switch (type) {
      case 'facebook':
        return new facebookService();
      case 'db':
        return new backendService(this.http);
      default:
        throw new Error('Authentication service not found');
    }
  }
}
