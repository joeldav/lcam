module.exports = {
    "plugins": [
        "protractor"
    ],
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "sourceType": "module"
    },
    "rules": {
        "no-process-arg": "off",
        "no-process-exit": "off",
        "no-process-env": "off",
        "no-process-argv": "off",
        "no-console": "off",
        "linebreak-style": [
            "error",
            "unix"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};
